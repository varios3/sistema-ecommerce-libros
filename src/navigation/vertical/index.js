export default [
  {
    title: 'Home',
    route: 'home',
    icon: 'HomeIcon',
  },
  {
    title: 'Libros',
    route: 'book',
    icon: 'BookOpenIcon',
  },
  {
    title: 'Tienda',
    route: 'shop',
    icon: 'ShoppingBagIcon',
  },
  {
    title: 'Usuarios',
    icon: 'UserIcon',
    route: 'user',
  },
];
